﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using FacebookProjectTeam.Models;

namespace FacebookProjectTeam.Repository
{
    public class FacebookDbContext : DbContext
    {
        public FacebookDbContext(DbContextOptions<FacebookDbContext> options) : base(options)
        {

        }
        public DbSet<UserModels> TB_User { get; set; }
    }
}
